public class User implements Cloneable {
    private String username;
    private String password;

    public User(String username, String password) {
        this.username = new String(username);
        this.password = encrypt(password);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        result = prime * result
                + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

    private String encrypt(String password) {
        char[] pass = password.toCharArray();
        for (int i = 0; i < pass.length; ++i) {
            pass[i] = (char) (pass[i] ^ 'S');
        }
        return new String(pass);
    }

    public String getUsername() {
        return username;
    }

    @Override
    public User clone() {
        try {
            User clone = (User) super.clone();
            clone.username = this.username;
            clone.password = this.password;
            return clone;

        } catch (CloneNotSupportedException e) {
            System.out.println("Cloning not allowed.");
            return this;
        }
    }

    @Override
    public String toString() {
        return "Hello, {@" + username + "}, your password is {" + password + "}";
    }
}
