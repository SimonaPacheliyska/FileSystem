
public abstract class FileType implements Comparable<FileType> {
    protected String name;
    protected String absolutePath;
    protected Directory parent;

    protected FileType(String absolutePath, Directory parent) {
        this.absolutePath = new String(absolutePath);
        String[] splitted = absolutePath.split("/");
        if (splitted.length > 0) {
            name = splitted[splitted.length - 1];
        } else {
            name = new String("/");
        }
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }
    
    public Directory getParent() {
        return new Directory(this.absolutePath,this.parent);
    }

}
