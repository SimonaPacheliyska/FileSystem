import java.util.ArrayList;
import java.util.List;

public class FileSystemTree {

    private class Node implements Comparable<Node> {
        private FileType fileType;
        private List<Node> subDirs;

        Node(FileType fileType) {
            this.fileType = fileType;
        }

        @Override
        public int compareTo(Node o) {
            return fileType.absolutePath.compareTo(o.fileType.absolutePath);
        }
    }

    private Node root;

    public FileSystemTree() {
        this.root = new Node(new Directory("/", null));
        this.root.subDirs = new ArrayList<>();
        this.root.subDirs.add(new Node(new Directory("/home", (Directory) this.root.fileType)));
    }

    public boolean search(String path) {
        return searchHelper(this.root, path);
    }

    public boolean add(String path) {
        if (!search(path)) {
            return true;
        }
        return false;
    }

    private boolean searchHelper(Node node, String path) {
        if (node.fileType.absolutePath.equals(path)) {
            return true;
        }
        for (int i = 0; node.subDirs.size() > 1
                && i < node.subDirs.size(); ++i) {
            if (node.subDirs.get(i).fileType.absolutePath.equals(path)) {
                return true;
            }
            return searchHelper(node.subDirs.get(i), path);
        }
        return false;
    }

    public static void main(String[] args) {
        // System.out.println(new FileSysHandler().getFullPath());
    }
}
