public class File extends FileType {
    private StringBuilder content;
    private int size;

    public File(String absolutePath, Directory parent) {
        super(absolutePath, parent);
    }

    public String getContent() {
        return content.toString();
    }

    public int getSize() {
        return size;
    }

    @Override
    public int compareTo(FileType o) {
        return this.absolutePath.compareTo(o.absolutePath);
    }
}
