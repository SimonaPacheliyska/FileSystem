
public class CommandParser {
    private String command;
    private String[] commandLine;
    private String location;

    private boolean isValidCommand(String command) {
        commandLine = command.split(" ");
        boolean isMatches = Commands.getCommands().toString().contains(commandLine[0]);
        if (!isMatches) {
            throw new IllegalArgumentException(
                    "Command " + "\"" + command + "\"" + "was not found");
        }
        return true;
    }

    public CommandParser(String command) {
        if (isValidCommand(command)) {
            this.command = new String(command);
            if (commandLine[1].contains("/")) {
                location = commandLine[1];
            }
        } else {
            this.command = " ";
            this.location = " ";
        }
    }
    
    public String getCommandLine() {
        return command;
    }

    public String getLocation() {
        return location;
    }

    public String getParentPath() {
        if (location.length() > 1 && location.contains("/")) {
            return location.substring(0, location.lastIndexOf("/"));
        }
        return " ";
    }

    public static void main(String[] args) {
        System.out.println(new CommandParser("cd /home/simona/Downloads").getCommandLine());
        //System.out.println(Commands.getCommands());
    }

}
