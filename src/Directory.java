
public class Directory extends FileType {
    public Directory(String absolutePath, Directory parent) {
        super(absolutePath,parent);
    }

    public Directory getParent() {
        return new Directory(parent.absolutePath, parent.parent);
    }

    @Override
    public int compareTo(FileType o) {
        return this.absolutePath.compareTo(o.absolutePath);
    }
}
