import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum Commands {
    cd,
    mkdir,
    create_file,
    cat,
    write,
    ls,
    exit;
    
    static Set<Commands> getCommands(){
        return new HashSet<Commands>(Arrays.asList(Commands.values()));
    }
}
